package com.adrian.elevator;

import java.util.List;

public class Elevator {
	
	private final RouteCalculator routeCalculator;
	
	public Elevator(RouteCalculator routeCalculator) {
		this.routeCalculator = routeCalculator;
	}

	public List<Integer> calculateRoute(InstructionSet instructionSet) {
		return routeCalculator.calculateRoute(instructionSet);
	}

	public int calculateJourneyLength(List<Integer> floors) {
		
		if (floors.size() <= 0) {
			return 0;
		}
		
		int total = 0;
		int lastFloor = floors.get(0); 

		for (Integer f : floors) {
			total += Math.abs(lastFloor - f);
			lastFloor = f; 
		}
		
		return total;
	}
}
