package com.adrian.elevator;

import java.util.List;

public class ElevatorMain {

	public enum ElavatorMode {MODE_A, MODE_B;}
	
	public static void main(String[] args) {
		if (args.length !=2) {
			usage();
		}
		
		( new ElevatorMain() ).init(args);
	}

	private void init(String[] args) {
		
		ElavatorMode mode = null;
		
		String modeStr = args[1];
		switch (modeStr) {
			case "A":
				mode = ElavatorMode.MODE_A;
				break;
			case "B":
				mode = ElavatorMode.MODE_B;
				break;
			default:
				usage();
		}
		
		String inputFilePath = args[0];
		
		InstructionInputParser parser = new InstructionInputParser();
		
		List<InstructionSet> instructionSets = null;
		try {
			instructionSets = parser.parseInputFile(inputFilePath);
		} catch (Exception e) {
			System.out.println("Unable to parse input file. ");
			e.printStackTrace();
			usage();
			return;
		}
		
		runApp(instructionSets, mode);
	}
	
	private void runApp(List<InstructionSet> instructionSets, ElavatorMode mode) {
		Elevator elevator = new Elevator(createNewRouteCalculator(mode));
		
		for (InstructionSet instructionSet : instructionSets) {
			List<Integer> output = elevator.calculateRoute(instructionSet);
			int journeyLength = elevator.calculateJourneyLength(output);
			System.out.println(Utils.listJoin(output) + "(" + journeyLength + ")");
		}
		
	}
	
	private RouteCalculator createNewRouteCalculator(ElavatorMode mode) {
		switch (mode) {
			case MODE_A:
				return new RouteCalculatorModeA();
			case MODE_B:
				return new RouteCalculatorModeB();
			default:
				usage();
				return null;
		}
	}

	public static void usage() {
		System.out.println("usage: java ElevatorMain <A|B> <file path>");
		System.exit(1);
	}
}
