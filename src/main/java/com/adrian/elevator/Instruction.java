package com.adrian.elevator;

public final class Instruction {

	private final int floorFrom;
	private final int floorTo;
	
	public Instruction(int floorFrom, int floorTo) {
		this.floorFrom = floorFrom;
		this.floorTo = floorTo;
	}

	public final int getFloorFrom() {
		return floorFrom;
	}

	public final int getFloorTo() {
		return floorTo;
	}	
}
