package com.adrian.elevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class InstructionInputParser {
	
	public List<InstructionSet> parseInputFile(String filePath) throws Exception {
	
		File file = new File(filePath);
		
		List<InstructionSet> instructionSets = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
		    while ((line = br.readLine()) != null ) {
		    	String trimmedLine = line.trim();
		       if (trimmedLine.isEmpty()) {
		    	   continue;
		       }
		       
		       instructionSets.add(parseLine(trimmedLine));
		    }
		}
		
		return instructionSets;
	}

	InstructionSet parseLine(String trimmedLine) throws Exception {
		
		String[] start_floor_arr = trimmedLine.split(":");
		if (start_floor_arr.length != 2) {
			throw new Exception("bad input format on line "+trimmedLine+". Must have one ':' char to signify start floor.");
		}

		int startFloor = Integer.parseInt(start_floor_arr[0]);
		
		if (startFloor < 0) {
			throw new Exception("start floor cannot be negative "+trimmedLine);
		}
		
		String routeInput = start_floor_arr[1];
		
		List<Instruction> instructions = new ArrayList<>();
		String [] singleInstructionInputs = routeInput.split(",");
		for (String singleInstructionInput : singleInstructionInputs) {
			instructions.add(parseSingleInstruction(singleInstructionInput));
		}
		
		return new InstructionSet(startFloor, instructions);
	}
	
	private Instruction parseSingleInstruction(String singleInstructionInput) throws Exception {
		String [] floorFromTo = singleInstructionInput.split("-");
		
		if (floorFromTo.length != 2) {
			throw new Exception("Badly formed instruction. "+singleInstructionInput);
		}
		
		int floorFrom = Integer.parseInt(floorFromTo[0]);
		int floorTo   = Integer.parseInt(floorFromTo[1]);
		
		if (floorFrom < 0 || floorTo < 0) {
			throw new Exception("Cannot have negative floor numbers. "+singleInstructionInput);
		}
		
		return new Instruction(floorFrom, floorTo);
	}
}
