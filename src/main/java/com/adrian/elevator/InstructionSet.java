package com.adrian.elevator;

import java.util.ArrayList;
import java.util.List;

public final class InstructionSet {

	private final List<Instruction> instructions;
	private final int startFloor;
	
	public InstructionSet(int startFloor, List<Instruction> instructions) {
		this.startFloor = startFloor;
		this.instructions = instructions;
	}

	public final int getStartFloor() {
		return startFloor;
	}

	public final List<Instruction> getInstructions() {
		return new ArrayList<Instruction>(instructions);
	}	
}
