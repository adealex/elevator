package com.adrian.elevator;

import java.util.List;

public interface RouteCalculator {

	List<Integer> calculateRoute(InstructionSet instructionSet);
}
