package com.adrian.elevator;

import java.util.ArrayList;
import java.util.List;

public class RouteCalculatorModeA implements RouteCalculator {

	public List<Integer> calculateRoute(InstructionSet instructionSet) {
		int startFloor = instructionSet.getStartFloor();
		List<Instruction> instructions = instructionSet.getInstructions();
		
		List<Integer> floorsOnRoute = new ArrayList<>();
		floorsOnRoute.add(startFloor);
		int mostRecentFloor = startFloor;
		for (Instruction instruction : instructions) {
			
			int floorFrom = instruction.getFloorFrom();
			int floorTo = instruction.getFloorTo();
			if (mostRecentFloor != floorFrom) {
				floorsOnRoute.add(floorFrom);
				mostRecentFloor = floorFrom;
			}
				
			if (floorFrom != floorTo) {
				floorsOnRoute.add(floorTo);
				mostRecentFloor = floorTo;
			}
		}
		
		return floorsOnRoute;
	}
}
