package com.adrian.elevator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RouteCalculatorModeB implements RouteCalculator {

	private enum Direction {UP, DOWN;}
	
	public List<Integer> calculateRoute(InstructionSet instructionSet) {
		int startFloor = instructionSet.getStartFloor();
		List<Instruction> instructions = instructionSet.getInstructions();
		
		List<Integer> floorsOnRoute = new ArrayList<>();
		floorsOnRoute.add(startFloor);
		
		int mostRecentFloor = startFloor;
		
		while (instructions.size()>0) {
		
			List<Integer> floorsForThisDirection = new ArrayList<>();
			Instruction instruction = instructions.remove(0);

			addFloorsToList(floorsForThisDirection, instruction);
			Direction direction = getDirection(instruction);
			
			while (instructions.size()>0 && getDirection(instructions.get(0)).equals(direction))  {
				Instruction absorbedInstruction = instructions.remove(0);
				addFloorsToList(floorsForThisDirection, absorbedInstruction);
			}
			
			floorsForThisDirection = removeDuplicatesAndSortForDirection(floorsForThisDirection, direction);
			
			if (mostRecentFloor == floorsForThisDirection.get(0)) {
				floorsForThisDirection.remove(0);
			}
			
			mostRecentFloor = floorsForThisDirection.get(floorsForThisDirection.size()-1);
			
			floorsOnRoute.addAll(floorsForThisDirection);
		}
		
		return floorsOnRoute;
	}
	
	private void addFloorsToList(List<Integer> floorsForThisDirection, Instruction instruction) {
		floorsForThisDirection.add(instruction.getFloorFrom());
		floorsForThisDirection.add(instruction.getFloorTo());
	}

	private List<Integer> removeDuplicatesAndSortForDirection(Collection<Integer> collection, Direction direction) {
		List<Integer> sortedNoDuplicates = Utils.removeDuplicatesAndSort(collection);
		
		if (direction.equals(Direction.UP)) {
			return sortedNoDuplicates;
		}
		
		Collections.reverse(sortedNoDuplicates);
		return sortedNoDuplicates;		
	}
	
	
	private Direction getDirection(Instruction instruction) {
		return ( instruction.getFloorFrom() < instruction.getFloorTo() ? Direction.UP : Direction.DOWN );
	}
}

