package com.adrian.elevator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {

	public static String listJoin(List<Integer> list) {
		StringBuffer sb = new StringBuffer();
		
		for (Integer floor : list) {
			sb.append(floor).append(" ");
		}
		
		return sb.toString();
	}
	

	public static Collection<Integer> removeDuplicates(Collection<Integer> list) {
		Set<Integer> st = new HashSet<>();
		st.addAll(list);
		
		return st;
	}
	

	public static List<Integer> removeDuplicatesAndSort(Collection<Integer> collection) {
		Collection<Integer> uniqueElementList = removeDuplicates(collection);
		
		List<Integer> list = new ArrayList<>();
		list.addAll(uniqueElementList);
		Collections.sort(list);
		
		return list;
	}
}
