package com.adrian.elevator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class ElevatorTest extends TestCase {

	private List<InstructionSet> instructions;

	@Override
	public void setUp() throws Exception {
		String test1 = "10:8-1";
		String test2 = "9:1-5,1-6,1-5";
		String test3 = "2:4-1,4-2,6-8";
		String test4 = "3:7-9,3-7,5-8,7-11,11-1";
		String test5 = "7:11-6,10-5,6-8,7-4,12-7,8-9";
		String test6 = "6:1-8,6-8";
		
		InstructionInputParser parser = new InstructionInputParser();
		
		instructions = new ArrayList<>();
		
		instructions.add(parser.parseLine(test1));
		instructions.add(parser.parseLine(test2));
		instructions.add(parser.parseLine(test3));
		instructions.add(parser.parseLine(test4));
		instructions.add(parser.parseLine(test5));
		instructions.add(parser.parseLine(test6));
		

	}
	
	public void testElevatorModeA() {
		ArrayList<Integer> expectedResultsTest1ModeA = new ArrayList<>();
		expectedResultsTest1ModeA.add(10);
		expectedResultsTest1ModeA.add(8);
		expectedResultsTest1ModeA.add(1);

		ArrayList<Integer> expectedResultsTest2ModeA = new ArrayList<>();
		expectedResultsTest2ModeA.add(9);
		expectedResultsTest2ModeA.add(1);
		expectedResultsTest2ModeA.add(5);
		expectedResultsTest2ModeA.add(1);
		expectedResultsTest2ModeA.add(6);
		expectedResultsTest2ModeA.add(1);
		expectedResultsTest2ModeA.add(5);

		ArrayList<Integer> expectedResultsTest3ModeA = new ArrayList<>();
		expectedResultsTest3ModeA.add(2);
		expectedResultsTest3ModeA.add(4);
		expectedResultsTest3ModeA.add(1);
		expectedResultsTest3ModeA.add(4);
		expectedResultsTest3ModeA.add(2);
		expectedResultsTest3ModeA.add(6);
		expectedResultsTest3ModeA.add(8);

		ArrayList<Integer> expectedResultsTest4ModeA = new ArrayList<>();
		expectedResultsTest4ModeA.add(3);
		expectedResultsTest4ModeA.add(7);
		expectedResultsTest4ModeA.add(9);
		expectedResultsTest4ModeA.add(3);
		expectedResultsTest4ModeA.add(7);
		expectedResultsTest4ModeA.add(5);
		expectedResultsTest4ModeA.add(8);
		expectedResultsTest4ModeA.add(7);
		expectedResultsTest4ModeA.add(11);
		expectedResultsTest4ModeA.add(1);

		ArrayList<Integer> expectedResultsTest5ModeA = new ArrayList<>();
		expectedResultsTest5ModeA.add(7);
		expectedResultsTest5ModeA.add(11);
		expectedResultsTest5ModeA.add(6);
		expectedResultsTest5ModeA.add(10);
		expectedResultsTest5ModeA.add(5);
		expectedResultsTest5ModeA.add(6);
		expectedResultsTest5ModeA.add(8);
		expectedResultsTest5ModeA.add(7);
		expectedResultsTest5ModeA.add(4);
		expectedResultsTest5ModeA.add(12);
		expectedResultsTest5ModeA.add(7);
		expectedResultsTest5ModeA.add(8);
		expectedResultsTest5ModeA.add(9);
		
		ArrayList<Integer> expectedResultsTest6ModeA = new ArrayList<>();
		expectedResultsTest6ModeA.add(6);
		expectedResultsTest6ModeA.add(1);
		expectedResultsTest6ModeA.add(8);
		expectedResultsTest6ModeA.add(6);
		expectedResultsTest6ModeA.add(8);
		
		
		
		Elevator elevator = new Elevator(new RouteCalculatorModeA());
		
		List<Integer> route0 = elevator.calculateRoute(instructions.get(0));
		assertEquals(expectedResultsTest1ModeA, route0);
		int journeyLength0 = elevator.calculateJourneyLength(route0);
		assertEquals(9, journeyLength0);
		
		List<Integer> route1 = elevator.calculateRoute(instructions.get(1));
		assertEquals(expectedResultsTest2ModeA, route1);
		int journeyLength1 = elevator.calculateJourneyLength(route1);
		assertEquals(30, journeyLength1);

		List<Integer> route2 = elevator.calculateRoute(instructions.get(2));
		assertEquals(expectedResultsTest3ModeA, route2);
		int journeyLength2 = elevator.calculateJourneyLength(route2);
		assertEquals(16, journeyLength2);
		
		List<Integer> route3 = elevator.calculateRoute(instructions.get(3));
		assertEquals(expectedResultsTest4ModeA, route3);
		int journeyLength3 = elevator.calculateJourneyLength(route3);
		assertEquals(36, journeyLength3);
		
		List<Integer> route4 = elevator.calculateRoute(instructions.get(4));
		assertEquals(expectedResultsTest5ModeA, route4);
		int journeyLength4 = elevator.calculateJourneyLength(route4);
		assertEquals(40, journeyLength4);
		
		List<Integer> route5 = elevator.calculateRoute(instructions.get(5));
		assertEquals(expectedResultsTest6ModeA, route5);
		int journeyLength5 = elevator.calculateJourneyLength(route5);
		assertEquals(16, journeyLength5);
	}
	

	public void testElevatorModeB() {
		ArrayList<Integer> expectedResultsTest1ModeB = new ArrayList<>();
		expectedResultsTest1ModeB.add(10);
		expectedResultsTest1ModeB.add(8);
		expectedResultsTest1ModeB.add(1);
		
		ArrayList<Integer> expectedResultsTest2ModeB = new ArrayList<>();
		expectedResultsTest2ModeB.add(9);
		expectedResultsTest2ModeB.add(1);
		expectedResultsTest2ModeB.add(5);
		expectedResultsTest2ModeB.add(6);

		ArrayList<Integer> expectedResultsTest3ModeB = new ArrayList<>();
		expectedResultsTest3ModeB.add(2);
		expectedResultsTest3ModeB.add(4);
		expectedResultsTest3ModeB.add(2);
		expectedResultsTest3ModeB.add(1);
		expectedResultsTest3ModeB.add(6);
		expectedResultsTest3ModeB.add(8);

		ArrayList<Integer> expectedResultsTest4ModeB = new ArrayList<>();
		expectedResultsTest4ModeB.add(3);
		expectedResultsTest4ModeB.add(5);
		expectedResultsTest4ModeB.add(7);
		expectedResultsTest4ModeB.add(8);
		expectedResultsTest4ModeB.add(9);
		expectedResultsTest4ModeB.add(11);
		expectedResultsTest4ModeB.add(1);

		ArrayList<Integer> expectedResultsTest5ModeB = new ArrayList<>();
		expectedResultsTest5ModeB.add(7);
		expectedResultsTest5ModeB.add(11);
		expectedResultsTest5ModeB.add(10);
		expectedResultsTest5ModeB.add(6);
		expectedResultsTest5ModeB.add(5);
		expectedResultsTest5ModeB.add(6);
		expectedResultsTest5ModeB.add(8);
		expectedResultsTest5ModeB.add(12);
		expectedResultsTest5ModeB.add(7);
		expectedResultsTest5ModeB.add(4);
		expectedResultsTest5ModeB.add(8);
		expectedResultsTest5ModeB.add(9);

		ArrayList<Integer> expectedResultsTest6ModeB = new ArrayList<>();
		expectedResultsTest6ModeB.add(6);
		expectedResultsTest6ModeB.add(1);
		expectedResultsTest6ModeB.add(6);
		expectedResultsTest6ModeB.add(8);


		Elevator elevator = new Elevator(new RouteCalculatorModeB());
		
		List<Integer> route0 = elevator.calculateRoute(instructions.get(0));
		assertEquals(expectedResultsTest1ModeB, route0);
		int journeyLength0 = elevator.calculateJourneyLength(route0);
		assertEquals(9, journeyLength0);
		
		List<Integer> route1 = elevator.calculateRoute(instructions.get(1));
		assertEquals(expectedResultsTest2ModeB, route1);
		int journeyLength1 = elevator.calculateJourneyLength(route1);
		assertEquals(13, journeyLength1);

		List<Integer> route2 = elevator.calculateRoute(instructions.get(2));
		assertEquals(expectedResultsTest3ModeB, route2);
		int journeyLength2 = elevator.calculateJourneyLength(route2);
		assertEquals(12, journeyLength2);

		List<Integer> route3 = elevator.calculateRoute(instructions.get(3));
		assertEquals(expectedResultsTest4ModeB, route3);
		int journeyLength3 = elevator.calculateJourneyLength(route3);
		assertEquals(18, journeyLength3);

		List<Integer> route4 = elevator.calculateRoute(instructions.get(4));
		assertEquals(expectedResultsTest5ModeB, route4);
		int journeyLength4 = elevator.calculateJourneyLength(route4);
		assertEquals(30, journeyLength4);

		List<Integer> route5 = elevator.calculateRoute(instructions.get(5));
		assertEquals(expectedResultsTest6ModeB, route5);
		int journeyLength5 = elevator.calculateJourneyLength(route5);
		assertEquals(12, journeyLength5);

	}
}
